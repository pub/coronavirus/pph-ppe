# C19 Protective Positive Pressure Helmets (PPE-PPH)

Maybe one of the gnarliest aspects of this pandemic is the looming insecurity of frontline heathcare workers (and, eventually - those of us that will care for loved ones at home) who are exposed to heavy viral loads. The global shortage of masks is well known, as is their relative discomfort. Besides, N95 are just that - only 95% effective, and can have leaks at their edges. Healthcare workers have been routinely infected with C19 despite their use of existing PPE.

[Is this a good idea?](#tldr)

### What to Do

CPAP (constant positive airway pressure) is being used in treating patients, whose respiratory systems need a helping hand. In Europe, CPAP 'helmets' are available:

![baby-helmet](infant-cpap.jpg)

These helmets (not just for babies!) are fed a pressurized supply of air - Cesare Gregoretti mentioned using pressures here up to 40 cm H20 (although ~ 20 is more normal), with the addition of oxygenation to the flow, for patients with C19. These helmets are commercially available in Europe (though back-ordered) and not FDA approved in America.

The thought is to develop some low cost version of these helmets, as well as a small CPAP-like pump and filter, to deliver filtered air at positive pressure, for healthcare workers. Indeed, here is our boy Jude Law with such a device in the infamous Contagion film:

![jude](jude-law.jfif)

Hopefully, these would be more comfortable and more effective than N95 masks and goggles, as well as re-useable. In a longer run, and if desperate times cause hospitals to side-step regulations, these could be also used as CPAP helmets for patients with the disease. Since outgoing flow could also be filtered, patients wearing one such device could avoid shedding more virus into any atmosphere they share with others.

### Spec: Flow Rates, Pressures

Besides the actual helmet, I need to spec fans and filters. To start, Cesare was discussing some pressures for CPAP: he was quoting between 8 and 40 cmH20. That's an odd unit for yours truly, so:

| cm H20 | Pa (pascals) |
| --- | --- |
| 1 | 98.0665 |
| 8 | 800 |
| 40 | 4000 |

Also useful,

| litres / sec | cfm |
| --- | --- |
| 1 | 2.11888 |

For now, I am just aiming at reusable, comfortable PPE, not CPAP. So, the only pressure requirement is that *at no time is there negative pressure* within the helmet, and I also need to inflate it. To maintain positive pressure, I need to know how fast we can typically inhale, because I need to maintain positive pressure *while* also maintaining this flow rate into the lungs. For patients with ARDS, Bill quotes:

> To estimate tidal volume in patients with acute respiratory syndrome (ARDS), you will find that the volumes will range from 4 to 6 ml per kg ideal body weight(IBW).  
You can calculate IBW by  
IBW  
Estimated ideal body weight in (kg)  
Males: IBW = 50 kg + 2.3 kg for each inch over 5 feet.  
Females: IBW = 45.5 kg + 2.3 kg for each inch over 5 feet.  
Tidal Volumes will range in the clinic from 250 ml to 600 ml.  
The breath rates will be 13 to 30 Breaths per minute (BPM) depending on many factors.  The slower breath rates will show an inspiration to expiration time ratio (I:E ratio) of 1:3, the faster rates will exhibit 1:1 I:E ratio.   The ideal I:E ratio is 1:2.  
The peak flows will typically be around 60 Liters/min (LPM) or 1 liter/sec (lps)

Tables are nice,

| Label | values | unit | meaning |
| --- | --- | --- | --- |
| Tidal Volume (TV) | 0.5 - 3 | litres | volumetric size of one breath, lung capacity |
| Breathing Rate (BR) | 12 - 40 | breaths / minute | lower bound represents a relaxed state, more breaths / minute indicate exercise or distress |
| Minute Ventilation (VE) | TV x BR | litres / minute | total flow |

So, my practical engineering hat tells me to spec for the upper bounds here, so I'll calculate for:

| Tidal Volume | Breathing Rate | Inspiration : Exhalation | Instantaneous Flow |
| --- | --- | --- | --- | --- |
| 0.75 litres | 30 breaths / sec | 1 : 2 | 1.125 litres / sec (2.34 cfm) |

I can assume that seals are not perfect, but understanding how-imperfect they might be is probably the cinch on this thing. Otherwise, flows in to the helmet need to match the largest rate during an inhalation, and add to that enough pressure to accommodate for leaks in the mask, and whatever pressure is *also* needed to hold the helmet's structure. Filtration drops need to be overcome.

It is likely difficult to estimate leak-flow-rate, but I can probably find a quick and dirty model for flow rate, given pressure, through an orifice of some size. No time for that now, I'll take 1/2 of a sharp breath for 0.5 l/s for now, thanks.

For a pressure drop estimate, I found [this study](https://www.researchgate.net/publication/261745047_Respiratory_Source_Control_Using_Surgical_Masks_With_Nanofiber_Media) [pdf](2014_mask-pdrop.pdf) that estimates an upper bound of 0.3 cmH20 drop (30 Pa) for N95 masks. My hope is that filter modules would be on similar order.

**Then, specs would look like:**

| Interior Pressure | Filter Pressure Loss | Target Pressure |
| --- | --- | --- |
| 400 Pa | 30 Pa | 500 Pa (50 mmH20) |

| Heavy Breathing Flow | Leak Flow Loss Estimate | Target Flow |
| --- | --- | --- |
| 1.125 l/s | 0.5 l/s | 2 l/s (4.2 cfm) |

(for reference) 1000 Pa is ~ 0.15 PSI. This sounds appropriate. [Inflatable Arenas](https://en.wikipedia.org/wiki/Air-supported_structure) using air pressure commonly only need ~250 Pa to stay up, but this relationship is dependent on the size (bigger: smaller pressures).

Finally, I am here to investigate what kind of fan / pump I need. My burning desire / hope is that I can use commonly available '5015' blowers:

![5015](5015.jpg)

These are available in bulk on Amazon. To find proper specs, I can find a similar digikey part like [102-4391-ND](https://www.digikey.ca/product-detail/en/cui-devices/CBM-5015V-140/102-4391-ND/7622746) [pdf](5015-digikey.pdf).

![curve](5015-digikey-curve.png)

One is not enough, indeed ~ 6 of these would be necessary. Another note is that ~ 6 of these would be very loud. Not surprisingly, performance scales well with size for fans, as in this [digikey fan (90mm)](https://www.digikey.ca/product-detail/en/sanyo-denki-america-inc/9BMB12G201/1688-1375-ND/6192091) that single handedly surpasses our spec.

![bigboy](9BMB12G201_curve.png)

This is a ~ $30 part, and draws 20 Watts at full bore.

### Spec: Filtration

I can also surely find pressure drop through appropriate filter media.

> [from](https://www.ncbi.nlm.nih.gov/books/NBK554776/) [pdf](2020-01_covid-ncbi-bookshelf.pdf) Thus, SARS-CoV-2 belongs to the betaCoVs category. It has round or elliptic and often pleomorphic form, and a diameter of approximately 60–140 nm. Like other CoVs, it is sensitive to ultraviolet rays and heat.

| nm | microns |
| --- | --- |
| 60 | 0.06 |

However, the virus needs a droplet to move around in the air (Cesare mentioned this), or, more likely - it is typically carried out of a patient *on* a droplet, which (Cesare quote) are typically 40 microns. HEPA rated filters will remove down to 0.3 micron diameter elements, so we hope these are enough.

### Modular Connectors

A [spec](breathing_tubes_ISO5367_2014_.pdf) exists for *breathing circuit* interconnect. These look like ~ 20mm OD press fit tubes. Adhering to this standard will mean that any helmet / pump we make will be interoperable with existing hardware kit in hospitals. It sounds like this is global. Nice.

[Connector Drawing for ISO 5356-1:2015](breathing_tubes_conn_ISO_5356-1__2015_.pdf)

### One-Way Outlet Valve

One ways can be pretty simple devices. When finding these inside of 3M respirators, note that they are *basically* just a passive flap. This could be a simple 3D Printed Part.

- Modular: add filtered outlet for patients (don't contaminate the hospital)

### Fog ?

My thought / hope is that, because fresh air is being delivered into the helmet at all times, fog will not be an issue. It does not appear to pose a major problem in the designs above. An inlet could be situated near the mouth, and printed in such a way that air flows over the surface of the helmet - similar to a car's defrost fan - to avoid this.

### Humidification

Indeed, it's actually better to humidify air (for patients) as lower viscosity mucus is easier for the airway 'elevator' (cilia) to transport north. Here would be an addn'l module, in-line with input, probably after filtration. Ultrasonic humidifiers exist, but that would be a bigger project. Might be as simple as passing air over some water...

### Power / Fan Unit

This would be non-trivially large, probably on the order of 90x200x40mm box. Scrubs don't have pockets. Since the helmet is secured to the head (under-arm straps... tough design issue), presumably we can hang this off of the helmet...

With a 20 Watt draw from the fan, the battery will need to be ~ 160 Watt Hours to last for one shift. For reference, a standard power drill battery is only 60 Watt Hours.

### Materials and Methods

#### The Helmet

PET film looks likely: [McMaster](https://www.mcmaster.com/8567k52)

#### The Neck Juncture

It looks like these use a different, more-stretchy kind of plastic. Not sure. Helmet in the 1st picture here uses an inflated donut to seal against the neck, seems like a good idea.

#### The Neck Ring

I expect this can be 3D Printed. The Prusa's bed is just large enough to print a hoop large enough for me to comfortably fit my (large) head through.

#### Heat Welding

PET film can be heat welded. I'll see about finding one of these machines to make the first bag.

# Is This a Good Idea? {#tldr}

After having a run at the design spec, I think:

 - these might cost ~ $80 in parts, total.
 - it would take ~ 2 weeks concerted effort to prototype \#1
 - they would take ~ 0.2 person-days to assemble, and ~ 1 printer-day to fabricate
 - they might work better and be more comfortable than existing PPE, but
 - they aren't approved anywhere, and deploying this medium-complexity device entails, well, medium complexity.
 - to scale, most are injection molded parts. assembly and heat-sealing are more difficult.

Power

 - probably the toughest bit here is delivering power. I need to find an existing battery-and-charger solution and, again, this is not necessarily an aspect that scales easily at the edges. Perhaps if fans were ~ 5v, USB power banks could be used: or, usb power infrastructure and boost converters.

CPAP or PPE?

 - The overall complexity of a CPAP capable helmet would be roughly on-order with the PPE version, but an increase in cost mostly at the fan component, and power use (tying the device to wall power).
